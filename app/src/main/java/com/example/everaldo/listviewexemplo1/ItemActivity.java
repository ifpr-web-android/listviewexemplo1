package com.example.everaldo.listviewexemplo1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ItemActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        Intent intent = getIntent();
        String nomeItem = intent.getStringExtra(MainActivity.ITEM);

        TextView item = (TextView) findViewById(R.id.item);
        item.setText(nomeItem);

    }

    public void fechar(View view) {
        finish();
    }
}
