package com.example.everaldo.listviewexemplo1;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends ListActivity implements AdapterView.OnItemClickListener {

    public static final String ITEM = "item";
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,
                Arrays.asList(getResources().getStringArray(R.array.itens))));
        ListView items = getListView();
        items.setOnItemClickListener(this);


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, Integer.toString(position));
        TextView item = (TextView) view;
        Intent intent = new Intent(this, ItemActivity.class);
        intent.putExtra(ITEM, item.getText());
        startActivity(intent);
    }
}
